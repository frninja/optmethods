﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OptMethods
{
    public static class Algs
    {
        public static ExtremumResult Gauss(Epsilon eps)
        {
			return binFunc.Min(new GaussExtremumFinderStrategy(eps, new Point2D(0, 0), gaussX, gaussY));
        }

        public static ExtremumResult Rosenbrok(Epsilon eps)
        {
			return binFunc.Min(new RosenbrokExtremumFinderStrategy(eps,
				new Point2D(0, 0),
				AlphaBeta.Create(0, 1),
				rosenLambda2));
        }

		public static ExtremumResult FastestDescent(Epsilon eps)
		{
			return binFunc.Min(new FastestDescentExtremumFinderStrategy(eps,
                new Point2D(0, 0),
                alpha,
                beta,
                rosenLambda2));
		}

		public static ExtremumResult ConjugateGradient(Epsilon eps)
		{
            return binFunc.Min(new ConjugateGradientExtremumFinderStrategy(eps,
                new Point2D(0, 0),
                alpha,
                beta,
                rosenLambda2));
		}

		
		public static ExtremumResult PairTryMin(Epsilon eps) {
			return binFunc.Min(new PairExtremumFinderStrategy(eps,
				Point2D.Create(0, 0),
				4,
				1.0));
		}

		#region Funcs
		
		private readonly static Func<double, double, double> binFunc =
            // x^2 + xy + y^2 + x + y + 1
            (double x, double y) => x * x + x * y + y * y + x + y + 1;

        private static double gaussX(double y)
        {
            return -(1 + y) / 2;
        }

        private static double gaussY(double x)
        {
            return -(1 + x) / 2;
        }

		private readonly static Func<Point2D, AlphaBeta, double> rosenLambda2 =
			(Point2D point, AlphaBeta ab) => rosenLambda(point.X, point.Y, ab.Alpha, ab.Beta);

        private readonly static Func<double, double, double, double, double> rosenLambda =
            (double x, double y, double alpha, double beta) =>
                -(2 * alpha * x + alpha * y + beta * x + 2 * beta * y + alpha + beta)
                 /
                (2 * alpha * alpha + 2 * alpha * beta + 2 * beta * beta);

        private readonly static Func<Point2D, double> alpha =
            (Point2D point) => Alpha(point.X, point.Y);

        private readonly static Func<Point2D, double> beta =
            (Point2D point) => Beta(point.X, point.Y);

        private static double Alpha(double x, double y)
        {
            return 2 * x - y + 1;
        }

        private static double Beta(double x, double y)
        {
            return x + 2 * y + 1;
        }

        #endregion

    }
}
