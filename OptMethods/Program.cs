﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace OptMethods
{
    class Program
    {

        static double F1(double x)
        {
            return Math.Sin(x);
        }

        static void Main(string[] args)
        {
            Func<double, double> f1 = x => Math.Sin(x);

			/*Console.WriteLine(f1.LocalMax(new FibonacciLocalExtremumFinderStrategy(new Epsilon(0.0001))
                                          { Log = Console.WriteLine },
                                          Interval.Create(0, Math.PI)));*/

			/*Console.WriteLine(f1.LocalMax(new DichotomyLocalExtremumFinderStrategy(new Epsilon(0.0001), new Step(100)) 
                                                { Log = Console.WriteLine },
                Interval.Create(0, Math.PI)));*/

			/*Console.WriteLine(f1.LocalMax(new GoldenRatioLocalExtremumFinderStrategy(new Epsilon(0.0001),
                                                                                     new Step(100))
                                          { Log = Console.WriteLine },
                                          Interval.Create(0, Math.PI)));*/

			var extremumResult = Algs.PairTryMin(new Epsilon(0.0001));
            var res = new StringBuilder()
                .Append("Point: ")
                .AppendLine(extremumResult.Point.ToString())
                .Append("Value: ")
                .AppendLine(extremumResult.Value.ToString())
                .Append("Iterations: ")
                .AppendLine(extremumResult.IterationsCount.ToString())
                .ToString();

            Console.WriteLine(res);

            Console.ReadKey();
        }
    }
}
