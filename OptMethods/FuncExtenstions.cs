﻿using System;

namespace OptMethods
{
    public static class FuncExtenstions
    {
        public static double LocalMin(this Func<double, double> func,
            ILocalMinFinderStrategy<double> finderStrategy,
            Interval<double> interval)
        {
            return finderStrategy.LocalMin(interval, func);
        }

        public static double LocalMax(this Func<double, double> func,
            ILocalMaxFinderStrategy<double> finderStrategy,
            Interval<double> interval)
        {
            return finderStrategy.LocalMax(interval, func);
        }

		public static ExtremumResult Min(this Func<double, double, double> func, 
			IMinFinderStrategy<double> finderStrategy)
		{
			return finderStrategy.Min(func);
		}
    }
}
