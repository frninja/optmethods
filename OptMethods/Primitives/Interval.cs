﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace OptMethods
{
    public static class Interval
    {
        public static Interval<TElement> Create<TElement>(TElement left, TElement right)
            where TElement : IComparable<TElement>
        {
            if (left == null)
                throw new ArgumentNullException("left");
            if (right == null)
                throw new ArgumentNullException("right");

            return new Interval<TElement>(left, right);
        }
    }

    public struct Interval<TElement>
        where TElement : IComparable<TElement>
    {
        private readonly TElement _left;
        private readonly TElement _right;

        public TElement Left { get { return _left; } }
        public TElement Right { get { return _right; } }

        public Interval(TElement left, TElement right)
        {
            if (left == null)
                throw new ArgumentNullException("left");
            if (right == null)
                throw new ArgumentNullException("right");

            if (left.CompareTo(right) < 0)
            {
                _left = left;
                _right = right;
            }
            else
            {
                _left = right;
                _right = left;
            }
        }
    }
}
