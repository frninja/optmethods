﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace OptMethods
{
    public struct Point2D : IEquatable<Point2D>
    {
        private readonly double _x;
        private readonly double _y;

        public double X { get { return _x; } }
        public double Y { get { return _y; } }

        public Point2D(double x, double y)
        {
            _x = x;
            _y = y;
        }

		public static Point2D Create(double x, double y)
		{
			return new Point2D(x, y);
		}

		public Point2D Map(Func<double, double> mapX, Func<double, double> mapY)
		{
			return new Point2D(mapX(_x), mapY(_y));
		}

        public override string ToString()
        {
            return string.Format("({0} ; {1})", _x, _y);
        }

		public override int GetHashCode() {
			return X.GetHashCode() ^ Y.GetHashCode();
		}

		public bool Equals(Point2D other) {
			return X.Equals(other.X) && Y.Equals(other.Y);
		}

		public override bool Equals(object obj) {
			if (obj == null)
				return false;

			if (!(obj is Point2D))
				return false;

			return Equals((Point2D)obj);
		}

		public static bool operator==(Point2D left, Point2D right) {
			return left.Equals(right);
		}

		public static bool operator!=(Point2D left, Point2D right) {
			return !left.Equals(right);
		}
	}
}
