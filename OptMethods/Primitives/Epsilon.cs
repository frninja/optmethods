﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace OptMethods
{
    public struct Epsilon
    {
        private readonly double _value;

        public double Value { get { return _value; } }

        public Epsilon(double value)
        {
            if (value <= 0)
                throw new ArgumentException("Epsilon must be positive.");

            _value = value;
        }

        public static implicit operator double(Epsilon value)
        {
            return value.Value;
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Epsilon))
                return false;
            return Equals((Epsilon)obj);
        }

        public bool Equals(Epsilon other)
        {
            return this.Value.Equals(other.Value);
        }

        public static bool operator ==(Epsilon left, Epsilon right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Epsilon left, Epsilon right)
        {
            return !left.Equals(right);
        }
    }
}
