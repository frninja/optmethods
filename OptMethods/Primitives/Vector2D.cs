﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace OptMethods
{
	public struct Vector2D
	{
		private readonly Point2D _start;
		private readonly Point2D _end;

		private Vector2D(Point2D start, Point2D end)
		{
			_start = start;
			_end = end;
		}

		private Vector2D(Point2D end) : this(new Point2D(0, 0), end)
		{
		}

		public static Vector2D Create(Point2D start, Point2D end)
		{
			return new Vector2D(start, end);
		}

		public double Length
		{
			get
			{
				return Math.Sqrt((_start.X - _end.X) * (_start.X - _end.X)
							     + (_start.Y - _end.Y) * (_start.Y - _end.Y));
			}
		}
	}
}
