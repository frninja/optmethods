﻿using System;

namespace OptMethods
{
	public struct AlphaBeta
	{
		private readonly double _alpha;
		private readonly double _beta;

		public double Alpha { get { return _alpha; } }
		public double Beta  { get { return _beta; } }

        public static AlphaBeta Create(double alpha, double beta)
        {
            return new AlphaBeta(alpha, beta);
        }

        private AlphaBeta(double alpha, double beta)
		{
			_alpha = alpha;
			_beta = beta;
		}

        public AlphaBeta Map(Func<double, double> mapAlpha,
            Func<double, double> mapBeta)
        {
            return new AlphaBeta(mapAlpha(_alpha), mapBeta(_beta));
        }
        
		public double Norma
		{
			get { return Math.Sqrt(_alpha * _alpha + _beta * _beta); }
		}
	}
}
