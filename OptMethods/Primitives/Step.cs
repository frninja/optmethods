﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace OptMethods
{
    public struct Step : IEquatable<Step>
    {
        private readonly int _value;

        public int Value { get { return _value; } }

        public Step(int value)
        {
            if (value < 0)
                throw new ArgumentException("Step must be not negative.");

            _value = value;
        }

        public Step GetNext()
        {
            return new Step(_value + 1);
        }

        public static implicit operator int(Step value)
        {
            return value.Value;
        }

        public static explicit operator Step(int value)
        {
            return new Step(value);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Step))
                return false;
            return Equals((Step)obj);
        }

        public bool Equals(Step other)
        {
            return this.Value.Equals(other.Value);
        }

        public static bool operator==(Step left, Step right)
        {
            return left.Equals(right);
        }

        public static bool operator!=(Step left, Step right)
        {
            return !left.Equals(right);
        }
    }
}
