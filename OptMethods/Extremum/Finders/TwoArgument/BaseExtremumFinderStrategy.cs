﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace OptMethods
{
	public abstract class BaseExtremumFinderStrategy : IExtremumFinderStrategy<double>
	{
		private readonly Epsilon _epsilon;
		private readonly Point2D _startPoint;

		public Epsilon Epsilon { get { return _epsilon; } }
		public Point2D StartPoint { get { return _startPoint; } }
		//public Action<ExtremumResult> Log { get; set; }

		protected abstract ExtremumResult ExtremumCore(Func<double, double, double> func);

		protected BaseExtremumFinderStrategy(Epsilon epsilon, Point2D startPoint)
		{
			_epsilon = epsilon;
			_startPoint = startPoint;
		}

		public ExtremumResult Max(Func<double, double, double> func)
		{
			throw new NotImplementedException();
		}

		public ExtremumResult Min(Func<double, double, double> func)
		{
			return ExtremumCore(func);
		}
	}
}
