﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptMethods
{
    public class ConjugateGradientExtremumFinderStrategy : BaseExtremumFinderStrategy
    {
        private readonly Func<Point2D, AlphaBeta, double> _rosenLambda;
        private readonly Func<Point2D, double> _calcAlpha;
        private readonly Func<Point2D, double> _calcBeta;



        public Func<Point2D, AlphaBeta, double> RosenLambda
        {
            get { return _rosenLambda; }
        }

        public Func<Point2D, double> CalcAlpha
        {
            get { return _calcAlpha; }
        }

        public Func<Point2D, double> CalcBeta
        {
            get { return _calcBeta; }
        }



        public ConjugateGradientExtremumFinderStrategy(Epsilon epsilon,
            Point2D startPoint,
            Func<Point2D, double> calcAlpha,
            Func<Point2D, double> calcBeta,
            Func<Point2D, AlphaBeta, double> rosenLambda)
            : base(epsilon, startPoint)
        {
            _calcAlpha = calcAlpha;
            _calcBeta = calcBeta;
            _rosenLambda = rosenLambda;
        }

        protected override ExtremumResult ExtremumCore(Func<double, double, double> func)
        {
            Point2D prevPoint = StartPoint;
            Point2D currentPoint = StartPoint;

            AlphaBeta alphaBeta;
            double lambda;

            currentPoint = prevPoint;
            alphaBeta = AlphaBeta.Create(CalcAlpha(currentPoint),
                                         CalcBeta(currentPoint));

            Func<double, AlphaBeta, Epsilon, bool> canDoNextStep =
                (double lambdaArg, AlphaBeta alphaBetaArg, Epsilon epsilonArg) =>
                  Math.Sqrt(lambdaArg * lambdaArg *
                             (alphaBetaArg.Alpha * alphaBetaArg.Alpha
                             + alphaBetaArg.Beta * alphaBetaArg.Beta))
                    >=
                  epsilonArg;

            int iter = 0;
            do
            {
                ++iter;

                prevPoint = currentPoint;

                lambda = RosenLambda(prevPoint, alphaBeta);
                currentPoint = prevPoint.Map(x => x + lambda * alphaBeta.Alpha,
                                             y => y + lambda * alphaBeta.Beta);

                double af = CalcAlpha(currentPoint);
                double bf = CalcBeta(currentPoint);

                double w = (af * af + bf * bf)
                    /
                    (alphaBeta.Alpha * alphaBeta.Alpha + alphaBeta.Beta * alphaBeta.Beta);

                alphaBeta = alphaBeta.Map(alpha => alpha * w + af,
                                          beta => beta * w + bf);
            }
            while (canDoNextStep(lambda, alphaBeta, Epsilon));

            return new ExtremumResult(point: currentPoint,
                value: func(currentPoint.X, currentPoint.Y),
                iterationsCount: iter);
        }
    }
}
