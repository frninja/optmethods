﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace OptMethods
{
	public abstract class AlphaBetaExtremumFinderStrategy : BaseExtremumFinderStrategy
	{
		private readonly AlphaBeta _alphaBeta;

		public AlphaBeta AlphaBeta { get { return _alphaBeta; } }

		protected AlphaBetaExtremumFinderStrategy(Epsilon epsilon,
			Point2D startPoint,
			AlphaBeta alphaBeta)
			: base(epsilon, startPoint)
		{
			_alphaBeta = alphaBeta;
		}
	}
}
