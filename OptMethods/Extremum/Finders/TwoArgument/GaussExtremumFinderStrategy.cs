﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace OptMethods
{
	public class GaussExtremumFinderStrategy : BaseExtremumFinderStrategy
	{
		private readonly Func<double, double> _nextGaussX;
		private readonly Func<double, double> _nextGaussY;

		public Func<double, double> NextGaussX { get { return _nextGaussX; } }
		public Func<double, double> NextGaussY { get { return _nextGaussY; } }

		public GaussExtremumFinderStrategy(Epsilon epsilon, 
			Point2D startPoint, 
			Func<double, double> nextGaussX,
			Func<double, double> nextGaussY)
			: base(epsilon, startPoint)
		{
			_nextGaussX = nextGaussX;
			_nextGaussY = nextGaussY;
		}

		protected override ExtremumResult ExtremumCore(Func<double, double, double> func)
		{
			Point2D prevPoint = StartPoint;
			Point2D currentPoint = StartPoint;

			Func<Point2D, Point2D, bool> canContinue =
				(Point2D start, Point2D end) => Vector2D.Create(start, end).Length >= Epsilon;

			int iter = 0;
			do
			{
				++iter;

				prevPoint = currentPoint;

				currentPoint = Point2D.Create(_nextGaussX(prevPoint.Y),
											  _nextGaussY(currentPoint.X));
			}
			while (canContinue(prevPoint, currentPoint));

			return new ExtremumResult(point: currentPoint,
				value: func(currentPoint.X, currentPoint.Y),
				iterationsCount: iter);
		}
	}
}
