﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptMethods
{
	public class PairExtremumFinderStrategy : BaseExtremumFinderStrategy
	{
		private readonly int _directionsCount;
		private readonly double _stepLength;

		private readonly IList<double> _alpha;
		private readonly IList<double> _beta;

		public int DirectionsCount { get { return _directionsCount; } }
		public double StepLength { get { return _stepLength; } }

		public IList<double> Alpha { get { return _alpha; } }
		public IList<double> Beta { get { return _beta; } }



		public PairExtremumFinderStrategy(Epsilon epsilon,
			Point2D startPoint,
			int directionsCount,
			double stepLength) 
			: base(epsilon, startPoint) 
		{
			_directionsCount = directionsCount;
			_stepLength = stepLength;

			_alpha = new List<double>();
			_beta = new List<double>();
			for (var i = 0; i < directionsCount; ++i) {
				_alpha.Add(Math.Cos(Math.PI * i / directionsCount));
				_beta.Add(Math.Sin(Math.PI * i / directionsCount));
			}
		}



		protected override ExtremumResult ExtremumCore(Func<double, double, double> func)
		{
			Func<Point2D, double> funcAdapter = (Point2D point) => func(point.X, point.Y);

			Point2D currentPoint = StartPoint;
			Point2D prevPoint;

			double stepLength = StepLength;
			
			int iter = 0;
			do {
				++iter;
				prevPoint = currentPoint;
				currentPoint = FindNextPoint(stepLength, currentPoint, funcAdapter);

				bool directionFound = currentPoint != prevPoint;
				if (!directionFound) {
					stepLength /= 2;
				}
			}
			while (stepLength >= Epsilon);

			return new ExtremumResult(point: currentPoint,
									  value: funcAdapter(currentPoint),
									  iterationsCount: iter);
		}

		private Point2D FindNextPoint(double stepLength,
			Point2D currentPoint,
			Func<Point2D, double> funcAdapter) 
		{
			for (var i = 0; i < DirectionsCount; i++) {
				double currentValue = funcAdapter(currentPoint);

				double stepX = stepLength * Alpha[i];
				double stepY = stepLength * Beta[i];

				Point2D forwardPoint = MoveForward(currentPoint, stepX, stepY);
				Point2D backwardPoint = MoveBackward(currentPoint, stepX, stepY);

				double forwardValue = funcAdapter(forwardPoint);
				double backwardValue = funcAdapter(backwardPoint);

				bool minimumFound = forwardValue < currentValue || backwardValue < currentValue;

				if (minimumFound) {
					currentPoint = (forwardValue <= backwardValue) ? forwardPoint : backwardPoint;
				}
			}

			return currentPoint;
		}



		private static Point2D MoveForward(Point2D point, double deltaX, double deltaY) {
			return point.Map(x => x + deltaX, y => y + deltaY);
		}



		private static Point2D MoveBackward(Point2D point, double deltaX, double deltaY) {
			return point.Map(x => x - deltaX, y => y - deltaY);
		}

	}
}
