﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace OptMethods
{
	public class RosenbrokExtremumFinderStrategy : AlphaBetaExtremumFinderStrategy
	{
		private readonly Func<Point2D, AlphaBeta, double> _rosenLambda;

		public Func<Point2D, AlphaBeta, double> RosenLambda
		{
			get { return _rosenLambda; }
		}

		public RosenbrokExtremumFinderStrategy(Epsilon epsilon,
			Point2D startPoint,
			AlphaBeta alphaBeta,
			Func<Point2D, AlphaBeta, double> rosenLambda) 
			: base(epsilon, startPoint, alphaBeta)
		{
			_rosenLambda = rosenLambda;
		}

		protected override ExtremumResult ExtremumCore(Func<double, double, double> func)
		{
			AlphaBeta alphaBeta = AlphaBeta;

			Point2D prevPoint = StartPoint;
			Point2D currentPoint = StartPoint;
			
			int iter = 0;

			do
			{
				iter++;

				prevPoint = currentPoint;

				double lambda = _rosenLambda(prevPoint, alphaBeta);

				prevPoint = prevPoint.Map(x => x + lambda * alphaBeta.Alpha,
										  y => y + lambda * alphaBeta.Beta);

				alphaBeta = AlphaBeta.Create(-alphaBeta.Beta, alphaBeta.Alpha);
				lambda = _rosenLambda(prevPoint, alphaBeta);

				currentPoint = prevPoint.Map(x => x + lambda * alphaBeta.Alpha,
											 y => y + lambda * alphaBeta.Beta);

				alphaBeta = AlphaBeta.Create(currentPoint.X - prevPoint.X,
											 currentPoint.Y - prevPoint.Y);
			}
			while (alphaBeta.Norma >= Epsilon);

			return new ExtremumResult(point: currentPoint,
				value: func(currentPoint.X, currentPoint.Y),
				iterationsCount: iter);
		}
	}
}
