﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace OptMethods
{
	public interface IExtremumFinderStrategy<TElement> :
		IMaxFinderStrategy<TElement>,
		IMinFinderStrategy<TElement>
		where TElement : IComparable<TElement>
	{
	}

	public interface IMaxFinderStrategy<TElement>
		where TElement : IComparable<TElement>
	{
		ExtremumResult Max(Func<TElement, TElement, TElement> func);
	}

	public interface IMinFinderStrategy<TElement>
		where TElement : IComparable<TElement>
	{
		ExtremumResult Min(Func<TElement, TElement, TElement> func);
	}
}
