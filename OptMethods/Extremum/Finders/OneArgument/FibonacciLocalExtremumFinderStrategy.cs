﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptMethods
{
    public class FibonacciLocalExtremumFinderStrategy : BaseLocalExtremumFinderStrategy
    {
        public FibonacciLocalExtremumFinderStrategy(Epsilon epsilon)
            : base(epsilon)
        {
        }

        protected override double ExtremumCore(Interval<double> interval,
            ExtremumKind kind,
            Func<double, double> func)
        {
            var fibs = CreateFibbonacciList(interval, Epsilon);

            int n = fibs.Count - 1;

            LogCore(n);

            double a = interval.Left;
            double b = interval.Right;

            if (n == 1)
                return (b + a) / 2;

            double x1 = a + (b - a) * fibs[n - 2] / fibs[n];
            double x2 = a + (b - a) * fibs[n - 1] / fibs[n];

            double y1 = func(x1);
            double y2 = func(x2);

            while (n > 2)
            {
                --n;
                if (y1 >= y2 && kind == ExtremumKind.Max || y2 >= y1 && kind == ExtremumKind.Min)
                {
                    b = x2;
                    x2 = x1;
                    x1 = a + (b - a) * fibs[n - 2] / fibs[n];
                    y2 = y1;
                    y1 = func(x1);
                }
                else
                {
                    a = x1;
                    x1 = x2;
                    x2 = a + (b - a) * fibs[n - 1] / fibs[n];
                    y1 = y2;
                    y2 = func(x2);
                }
            }
            return (x2 + x1) / 2;
        }

        private IList<int> CreateFibbonacciList(Interval<double> interval, Epsilon eps)
        {
            double a = interval.Left;
            double b = interval.Right;

            var fibs = new List<int> { 0, 1 };
            int i = 1;

            while (fibs[i] < (b - a) / (eps * 2))
            {
                i++;
                fibs.Add(fibs[i - 1] + fibs[i - 2]);
            }

            return fibs.AsReadOnly();
        }
    }

}
