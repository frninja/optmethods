﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace OptMethods
{
    public abstract class StepLocalExtremumFinderStrategy : BaseLocalExtremumFinderStrategy
    {
        private readonly Step _maxStep;

		public Step MaxStep { get { return _maxStep; } }

        protected StepLocalExtremumFinderStrategy(Epsilon epsilon, Step maxStep)
            : base(epsilon)
        {
            _maxStep = maxStep;
        }
    }
}
