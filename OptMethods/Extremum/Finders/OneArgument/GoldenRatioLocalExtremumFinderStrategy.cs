﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace OptMethods
{
    public class GoldenRatioLocalExtremumFinderStrategy : StepLocalExtremumFinderStrategy
    {
        public GoldenRatioLocalExtremumFinderStrategy(Epsilon delta, Step maxStep) 
            : base(delta, maxStep)
        {
        }

        protected override double ExtremumCore(Interval<double> interval,
            ExtremumKind kind,
            Func<double, double> func)
        {
            double a = interval.Left;
            double b = interval.Right;

            Step step = (Step)0;
            Func<double> getGoldenRation = () => (Math.Sqrt(5) - 1) * (b - a) / 2;

            double x1 = b - getGoldenRation();
            double x2 = a + getGoldenRation();

            Func<Step, bool> isNextStepAvailable =
                (currentStep) => Math.Abs(b - a) >= 2 * Epsilon && currentStep < MaxStep;

            Func<double> getResult = () => (a + b) / 2;

            int iter = 0;
            while (isNextStepAvailable(step))
            {
                ++iter;

                step = step.GetNext();

                if (func(x1) >= func(x2) && kind == ExtremumKind.Max || func(x2) >= func(x1) && kind == ExtremumKind.Min)
                {
                    b = x2;
                    x2 = x1;
                    x1 = b - getGoldenRation();
                }
                else
                {
                    a = x1;
                    x1 = x2;
                    x2 = a + getGoldenRation();
                }

                LogCore(getResult());
            }
            LogCore(iter);

            return getResult();
        }
    }
}
