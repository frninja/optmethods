﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace OptMethods
{
    public abstract class BaseLocalExtremumFinderStrategy : ILocalExtremumFinderStrategy<double>
    {
        private readonly Epsilon _delta;

		public Epsilon Epsilon { get { return _delta; } }

        public Action<double> Log { get; set; }

        protected abstract double ExtremumCore(Interval<double> interval,
            ExtremumKind kind,
            Func<double, double> func);

        protected BaseLocalExtremumFinderStrategy(Epsilon delta)
        {
            _delta = delta;
        }

        public double LocalMax(Interval<double> interval, Func<double, double> func)
        {
            return ExtremumCore(interval, ExtremumKind.Max, func);
        }

        public double LocalMin(Interval<double> interval, Func<double, double> func)
        {
            return ExtremumCore(interval, ExtremumKind.Min, func);
        }

        protected void LogCore(double arg)
        {
            if (Log != null)
                Log(arg);
        }
    }
}
