﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace OptMethods
{
    public class DichotomyLocalExtremumFinderStrategy : StepLocalExtremumFinderStrategy
    { 
        public DichotomyLocalExtremumFinderStrategy(Epsilon delta, Step maxStep)
            :  base(delta, maxStep)
        {
        }

        protected override double ExtremumCore(Interval<double> interval, 
            ExtremumKind kind,
            Func<double, double> func)
        {
            double left = interval.Left;
            double right = interval.Right;

            Step step = (Step)0;

            Func<Step, bool> isNextStepAvailable =
                (currentStep) => Math.Abs(right - left) >= 2 * Epsilon && currentStep < MaxStep;

            int iter = 0;
            while (isNextStepAvailable(step))
            {
                ++iter;

                double x1 = (left + right - Epsilon) / 2;
                double x2 = (left + right + Epsilon) / 2;
                step = step.GetNext();
                if (func(x1) >= func(x2) && kind == ExtremumKind.Max || func(x2) >= func(x1) && kind == ExtremumKind.Min)
                    right = x2;
                else
                    left = x1;
            }

            LogCore(iter);

            return (left + right) / 2;
        }
    }
}
