﻿using System;

namespace OptMethods
{
    public interface ILocalExtremumFinderStrategy<TElement> :
        ILocalMaxFinderStrategy<TElement>,
        ILocalMinFinderStrategy<TElement>
        where TElement : IComparable<TElement>
    {
    }

    public interface ILocalMaxFinderStrategy<TElement>
        where TElement : IComparable<TElement>
    {
        TElement LocalMax(Interval<TElement> interval, Func<TElement, TElement> func);
    }

    public interface ILocalMinFinderStrategy<TElement>
        where TElement : IComparable<TElement>
    {
        TElement LocalMin(Interval<TElement> interval, Func<TElement, TElement> func);
    }
}
