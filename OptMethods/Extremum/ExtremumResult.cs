﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace OptMethods
{
    public struct ExtremumResult
    {
        private readonly Point2D _point;
        private readonly double _value;
        private readonly int _iterationsCount;

        public Point2D Point { get { return _point; } }
        public double  Value { get { return _value; } }
        public int     IterationsCount { get { return _iterationsCount; } }

        public ExtremumResult(Point2D point, double value, int iterationsCount)
        {
            _point = point;
            _value = value;
            _iterationsCount = iterationsCount;
        }
    }
}
